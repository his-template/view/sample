import { Component, EventEmitter, Input, Output } from '@angular/core';
import { NgFor } from '@angular/common';
import { AppStore } from '@his-viewmodel/app-store/src';
import { InputTextModule } from 'primeng/inputtext';
import { CheckboxModule } from 'primeng/checkbox';
import { DropdownModule } from 'primeng/dropdown';
import { FormsModule } from '@angular/forms';
import { DividerModule } from 'primeng/divider';
import { TriStateCheckboxModule } from 'primeng/tristatecheckbox';
import { DialogModule } from 'primeng/dialog';
import { ButtonModule } from 'primeng/button';

const primeGroup = [InputTextModule, CheckboxModule, DropdownModule, DialogModule, ButtonModule, DividerModule, TriStateCheckboxModule]
@Component({
  selector: 'his-app-store-editor-document',
  standalone: true,
  imports: [NgFor, FormsModule, primeGroup],
  templateUrl: './app-store-editor-document.component.html',
  styleUrls: ['./app-store-editor-document.component.scss']
})
export class AppStoreEditorDocumentComponent {

  @Input() appStore!: AppStore;
  @Output() appStoreChange = new EventEmitter<AppStore>();

  selectedIcon = '';
  imagePaths = ['/assets/image/image1.png', '/assets/image/image2.png', '/assets/image/image3.png', '/assets/image/image4.png', '/assets/image/image5.png', '/assets/image/image6.png', '/assets/image/image7.png', '/assets/image/image8.png', '/assets/image/image9.png'];
  systemUsers = [0, 1, 2, 3];
  programs = ['Angular', 'AngularJs', 'Python', 'Go'];
  dialogVisible = false;

  /**
   * 彈窗樣式
   * @memberof AppStoreEditorDocumentComponent
   */
  get dialogStyle() {

    return { width: '35vw', height: '80vh' };
  }

  /**
   * 點擊圖片
   * @memberof AppStoreEditorDocumentComponent
   */
  onIconClick() {

    this.dialogVisible = true;
  }

  /**
  * 選取圖片
  * @memberof AppStoreEditorDocumentComponent
  */
  onIconSelect(selectedIcon: string) {

    this.selectedIcon = selectedIcon;
  }

  /**
   * 按下確認鈕
   * @memberof AppStoreEditorDocumentComponent
   */
  onOkClick() {

    this.appStore.icon = this.selectedIcon;
    this.dialogVisible = false;
  }

  /**
   * 按下取消鈕
   * @memberof AppStoreEditorDocumentComponent
   */
  onCancelClick() {

    this.dialogVisible = false;
  }

}
