import { Component, EventEmitter, Input, Output } from '@angular/core';
import { NgTemplateOutlet } from '@angular/common';
import { ButtonModule } from 'primeng/button';
import { RadioButtonModule } from 'primeng/radiobutton';
import { ToolbarModule } from 'primeng/toolbar';

const primeGroup = [ButtonModule, RadioButtonModule, ToolbarModule];
@Component({
  selector: 'his-app-store-editor-toolbar',
  standalone: true,
  imports: [NgTemplateOutlet, primeGroup],
  templateUrl: './app-store-editor-toolbar.component.html',
  styleUrls: ['./app-store-editor-toolbar.component.scss']
})
export class AppStoreEditorToolbarComponent {

  @Input() tabViewIndex!: number;

  @Output() itemListClick: EventEmitter<void> = new EventEmitter();
  @Output() newItemClick: EventEmitter<void> = new EventEmitter();
  @Output() deleteItemClick: EventEmitter<void> = new EventEmitter();
  @Output() dittoItemClick: EventEmitter<void> = new EventEmitter();
  @Output() saveItemClick: EventEmitter<void> = new EventEmitter();


  /**
   * 點擊新增按鈕
   * @memberof AppStoreEditorToolbarComponent
   */
  onNewItemClick() {

    this.newItemClick.emit();
  }

  /**
   * 點擊Ditto按鈕
   * @memberof AppStoreEditorToolbarComponent
   */
  onDittoItemClick() {

    this.dittoItemClick.emit();
  }

  /**
   * 點擊清單按鈕
   * @memberof AppStoreEditorToolbarComponent
   */
  onItemListClick() {

    this.itemListClick.emit();
  }

  /**
   * 點擊儲存按鈕
   * @memberof AppStoreEditorToolbarComponent
   */
  onSaveItemClick() {

    this.saveItemClick.emit()
  }

  /**
   * 點擊刪除按鈕
   * @memberof AppStoreEditorToolbarComponent
   */
  onDeleteItemClick() {

    this.deleteItemClick.emit();
  }

  /**
   * 確認是否在document頁面
   * @memberof AppStoreEditorToolbarComponent
   */
  isDetail() {

    return this.tabViewIndex === 1;
  }



}

