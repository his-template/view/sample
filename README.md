# Sample (view)


## 專案說明
 - Angular CLI version 16.1.4.
 - expose {'./app-store-editor': './src/app/app.routes.ts'}
## 啟動專案
- 套件安裝 : "npm install"
- 繁中語系啟動 : "npm start"
- 英文語系啟動 : "npm run start:en"
- 簡中語系啟動 : "npm run start:cn"
- 繁中語系建置 : "ng build"
- 英文語系建置 : "ng build -c en-Us"
- 簡中語系建置 : "ng build -c zh-Hans"

## 使用專案
- 執行 ESlint 檢查 : "ng lint"
- 產生 compodoc 文件 : "npm run compodoc:build"
- 啟動 compodoc 文件 : "npm run compodoc:serve"
